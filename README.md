# wherefrom-compat

This package provides a set of stable interfaces to GHC's info provenance information API, 
namely the `wherefrom` function.

Each major version of this package corresponds to one version of this API, and will be 
kept up-to-date with newer versions of GHC for as long as possible.

This allows users of this API to be insulated from breaking changes to the API in `base` without
having to add CPP to their code.

