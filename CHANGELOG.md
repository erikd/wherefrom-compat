# Revision history for wherefrom-compat

## 0.1.1.0 -- 2024/02/01

* Fix interface around srcFile/srcSpan.

## 0.1.0.0 -- 2024/02/01

* First version. Released on an unsuspecting world.
